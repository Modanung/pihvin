/* Pihvin
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DOMINO_H
#define DOMINO_H

#include "luckey.h"

#include <Dry/Math/Polyhedron.h>

class Domino: public Object
{
    DRY_OBJECT(Domino, Object);

public:
    Domino(Context* context);
    void Draw(DebugRenderer* debug);

    Matrix3x4 GetTransform() const
    {
        return { origin_, rotation_, 1.f };
    }

    BoundingBox GetLocalBounds() const
    {
        const Vector3 halfSize{ .5f * size_ };
        BoundingBox bounds{ -halfSize + Vector3::RIGHT * halfSize.x_,
                             halfSize + Vector3::RIGHT * halfSize.x_};

        return bounds;
    }
    Polyhedron GetShape() const
    {
        return Polyhedron{ GetLocalBounds() }.Transformed(GetTransform());
    }
    Pair<Vector3, Vector3> GetSpine() const { return { origin_, origin_ + GetTransform().Rotation() * Vector3::RIGHT * size_.x_ }; }


private:
    Vector3 origin_;
    Vector3 size_;
    Quaternion rotation_;
};

struct Grid
{
    Grid(const Matrix3x4 transform, const Color& color = Color::GRAY):
        transform_{ transform },
        color_{ color }
    {}

    void Draw(DebugRenderer* debug, const Vector3 focus, float fade = 7.f)
    {
        const Vector3 normal{ transform_.Rotation() * Vector3::BACK };
        const Vector3 aX{ transform_.Rotation() * Vector3::RIGHT };
        const Vector3 aY{ transform_.Rotation() * Vector3::UP };
        const Vector3 origin{ transform_.Translation() };
        const Vector2 scale{ transform_.Scale().x_, transform_.Scale().y_ };
        const int nX{ Max(1, Abs(CeilToInt(fade / scale.x_)) / 2) };
        const int nY{ Max(1, Abs(CeilToInt(fade / scale.y_)) / 2) };
        Vector3 offset{ focus - origin.ProjectOntoPlane(normal, focus) };
        offset -= VectorMod(offset, { scale.x_, scale.y_, M_INFINITY });

        for (int x{ -nX }; x < nX; ++x)
        for (int y{ -nY }; y < nY; ++y)
        {
            const Vector2 cell{ x * 1.f, y * 1.f };
            const Vector3 start{ origin + offset +
                          aX * cell.x_ * scale.x_ +
                          aY * cell.y_ * scale.y_  };
            Vector3 to{ start + aX * scale.x_ };
            debug->AddLine(start, to,
                           { color_.Transparent(color_.a_ * PowN((fade - start.Lerp(to, .5f).DistanceToPoint(offset)) / fade, 3)) });
            to = { start + aY * scale.y_ };
            debug->AddLine(start, to,
                           { color_.Transparent(color_.a_ * PowN((fade - start.Lerp(to, .5f).DistanceToPoint(offset)) / fade, 3)) });
        }
    }

    Matrix3x4 transform_;
    Color color_;
};

struct Cursor3D
{
    Cursor3D(const Vector3& position, const Quaternion& rotation):
        position_{ position },
        rotation_{ rotation }
    {}

    void Draw(DebugRenderer* debug, float scale = 1.f)
    {
        const Vector3 aX{ rotation_ * Vector3::RIGHT };
        const Vector3 aY{ rotation_ * Vector3::UP };
        const Vector3 aZ{ rotation_ * Vector3::FORWARD };

        for (int i{ 0 }; i < 3; ++i)
        {
            Color c{ Color::WHITE };
            Vector3 d{};
            switch (i) {
            case 2: c = Color::RED;   d = aX; break;
            case 1: c = Color::GREEN; d = aY; break;
            case 0: c = Color::BLUE;  d = aZ; break;
            }
            Vector3 to{ position_ + d * scale };
            debug->AddLine(position_, to, c, false);
        }
    }

    Vector3 position_;
    Quaternion rotation_;
};

#endif // DOMINO_H
