/* Pihvin
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "domino.h"

Domino::Domino(Context* context): Object(context),
    origin_{},
    size_{ 4.f, 2.f, 1.f },
    rotation_{ 0.f, 0.f, 90.f }
{
}

#include "mastercontrol.h"
void Domino::Draw(DebugRenderer* debug)
{
    if (size_ == Vector3::ZERO)
        debug->AddSphere({ origin_, .1f }, Color::WHITE.Transparent(.25f), false);

    debug->AddPolyhedron(GetShape(), Color::YELLOW.Transparent(.125f));

    auto spine{ GetSpine() };
    debug->AddLine(spine.first_, spine.second_, Color::WHITE.Transparent(.25f), false);

    const Vector3 cursorPos{ 0, 0, 0 };
    Cursor3D{ cursorPos, rotation_ }.Draw(debug, MC->DistanceToCamera(cursorPos) * 4e-2f);
    const Vector3 focus{ cursorPos };
    const Color marine{ Color::AZURE.Lerp(Color{ .23f }, .5f).Transparent(3/3.f) };

    // Frontal grid
    Vector3 gridNormal{ rotation_ * Vector3::BACK };
    Color col{ marine.Transparent(marine.a_ * MC->ViewAligned(gridNormal)) };
    Grid{ { origin_, rotation_, Vector3::ONE }, col }
    .Draw(debug, focus);
    Grid{ { origin_, rotation_, Vector3::ONE * .125f }, col.Transparent(col.a_ * .25f) }
    .Draw(debug, focus);

    gridNormal = rotation_ * Quaternion{ 90.f, Vector3::UP } * Vector3::BACK;
    col = marine.Transparent(marine.a_ * MC->ViewAligned(gridNormal));
    // Base grid
    Grid{ { origin_, rotation_ * Quaternion{ 90.f, Vector3::UP }, Vector3::ONE }, col }
    .Draw(debug, focus);
    Grid{ { origin_, rotation_ * Quaternion{ 90.f, Vector3::UP }, Vector3::ONE * .125f }, col.Transparent(col.a_ * .25f) }
    .Draw(debug, focus);
}
