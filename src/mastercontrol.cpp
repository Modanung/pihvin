/* Pihvin
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "lexer.h"
#include "commandline.h"
#include "domino.h"
#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context *context): Application(context),
    scene_{ nullptr },
    camera_{ nullptr },
    domino_{ nullptr }
{
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "Pihvin.log";
    engineParameters_[EP_WINDOW_TITLE] = "Pihvin";
//    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Data;CoreData";
}

void MasterControl::Start()
{
    context_->RegisterSubsystem(this);
    context_->RegisterSubsystem<Lexer>();
    context_->RegisterFactory<Domino>();

    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());

    CreateScene();

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(MasterControl, HandlePostRenderUpdate));

    GetSubsystem<Input>()->SetMouseVisible(true);
    CommandLine* console{ context_->RegisterSubsystem<CommandLine>() };
    console->SetVisible(true);
    console->LogMessage("Welcome!");
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}

void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<PhysicsWorld>();
    scene_->CreateComponent<DebugRenderer>()->SetLineAntiAlias(true);

    domino_ = context_->CreateObject<Domino>();

    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(2.0f, 3.0f, 1.0f));
    lightNode->CreateComponent<Light>();
    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->LookAt(Vector3::FORWARD);
    cameraNode->SetWorldPosition(Vector3{ 0.f, 2.f, -10.f });
    camera_ =  cameraNode->CreateComponent<Camera>();
    camera_->SetNearClip(1e-3f);
    RENDERER->SetViewport(0, new Viewport(context_, scene_, camera_));
//    Box!
//    scene_->CreateChild()->CreateComponent<StaticModel>()->SetModel(CACHE->GetResource<Model>("Models/Box.mdl"));
}

void MasterControl::HandlePostRenderUpdate(StringHash eventType, VariantMap& evenData)
{
    domino_->Draw(scene_->GetComponent<DebugRenderer>());
}
