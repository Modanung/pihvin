/* Pihvin
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "lexer.h"

Lexer::Lexer(Context* context): Object(context)
{
    SubscribeToEvent(E_CONSOLECOMMAND, DRY_HANDLER(Lexer, HandleConsoleCommand));
}

int Lexer::Interpret(const String& str)
{
    DRY_LOGRAW(str);

    if (str.IsEmpty())
        return 0;

    return 0;
}

void Lexer::HandleConsoleCommand(StringHash eventType, VariantMap& eventData)
{
    using namespace ConsoleCommand;
//    if (eventData[P_ID].GetString() == GetTypeName())
        Interpret(eventData[P_COMMAND].GetString());
}
