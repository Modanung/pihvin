/* Pihvin
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#define MC GetSubsystem<MasterControl>()

namespace Dry {
class Node;
class Scene;
}

class Domino;

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);
public:
    MasterControl(Context* context);

    Scene* GetScene() const { return scene_; }

    // Setup before engine initialization. Modifies the engine paramaters.
    void Setup() override;
    // Setup after engine initialization.
    void Start() override;
    // Cleanup after the main loop. Called by Application.
    void Stop() override;
    void Exit();

    float DistanceToCamera(const Vector3& pos)
    {
        return camera_->GetNode()->GetWorldPosition().DistanceToPoint(pos);
    }

    float ViewAligned(const Vector3& direction)
    {
        return Clamp(Abs(camera_->GetNode()->GetWorldDirection().DotProduct(direction)) * 23.f, .5f, 1.f);
    }

private:
    void CreateScene();
    void HandlePostRenderUpdate(StringHash eventType, VariantMap& evenData);

    Scene* scene_;
    Camera* camera_;
    SharedPtr<Domino> domino_;
};

#endif // MASTERCONTROL_H
