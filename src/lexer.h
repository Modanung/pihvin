/* Pihvin
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef LEXER_H
#define LEXER_H

#include "luckey.h"

#include <string>
using namespace std;

class Lexer: public Object
{
    DRY_OBJECT(Lexer, Object);

public:
    Lexer(Context* context);

    int Interpret(const String& str);

    bool isOctal(char c) { return
                (c >= '0' && c <= '7') ||
                (c >= 'a' && c <= 'h'); }

    bool isOctalDigit(char c) { return isdigit(c) && isOctal(c); }
    bool isOctalAlpha(char c) { return islower(c) && isOctal(c); }

    int parseInt(const string& str, unsigned& pos)
    {
        unsigned at{ pos };
        unsigned len{ 0 };
        if (at >= str.size())
            return -1;

        bool isDigit{ isdigit(str.at(at)) != 0 };
        auto dig = [&](unsigned c) -> bool { return c < str.size() && (isDigit
                                                                       ? isOctalDigit(str.at(c))
                                                                       : isOctalAlpha(str.at(c))); };
        while (dig(at + len))
            ++len;

        pos = at + len;

        if (!len)
            return -1;

        if (isDigit)
        {
            return stoi(str.substr(at, len), 0, 8);
        }
        else
        {
            string num{ str.substr(at, len) };
            for (unsigned i{ 0 }; i < num.length(); ++i)
                num.at(i) = num.at(i) - 49;

            return stoi(num, 0, 8);
        }
    }

private:
    void HandleConsoleCommand(StringHash eventType, VariantMap& eventData);
};

#endif // LEXER_H
