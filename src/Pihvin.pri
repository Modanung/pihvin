HEADERS += \
    $$PWD/commandline.h \
    $$PWD/domino.h \
    $$PWD/lexer.h \
    $$PWD/luckey.h \
    $$PWD/mastercontrol.h \
    $$PWD/sceneobject.h \

SOURCES += \
    $$PWD/commandline.cpp \
    $$PWD/domino.cpp \
    $$PWD/lexer.cpp \
    $$PWD/luckey.cpp \
    $$PWD/mastercontrol.cpp \
    $$PWD/sceneobject.cpp \
